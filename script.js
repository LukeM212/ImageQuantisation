const steps = 4;
const input = document.getElementById("sourceImage");
const output = document.getElementById("quantisedImage");
const colours = [ ];

function index(x, y) {
	return 4 * ((input.width * y) + x);
}

document.getElementById("imageSelector").onchange = function () {
	input.hidden = output.hidden = false;
	let reader = new FileReader();
	reader.readAsDataURL(this.files[0]);
	reader.onload = e => {
		let tmpImg = new Image();
		tmpImg.src = e.target.result;
		tmpImg.onload = e => {
			let width = output.width = input.width = tmpImg.width;
			let height = output.height = input.height = tmpImg.height;
			let ctx = output.getContext('2d');
			ctx.drawImage(tmpImg, 0, 0);
			input.getContext('2d').drawImage(tmpImg, 0, 0);
			let imageData = ctx.getImageData(0, 0, width, height);
			let data = imageData.data;
			
			for (let y = 0; y < height; y++) {
				for (let x = 0; x < width; x++) {
					for (let c = 0; c < 3; c++) {
						let pixel = data[index(x, y) + c];
						data[index(x, y) + c] = Math.floor(Math.round(pixel * (steps - 1) / 255) * 255 / (steps - 1));
						let error = pixel - data[index(x, y) + c];
						
						if (x < width - 1)
							data[index(x + 1, y    ) + c] += error * 7 / 16;
						if (x > 0 && y < height - 1)
							data[index(x - 1, y + 1) + c] += error * 3 / 16;
						if (y < height - 1)
							data[index(x    , y + 1) + c] += error * 5 / 16;
						if (x < width - 1 && y < height - 1)
							data[index(x + 1, y + 1) + c] += error * 1 / 16;
					}
				}
			}
			ctx.putImageData(imageData, 0, 0);
		};
	};
};
